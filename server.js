var express = require('express');
var path = require('path');
var logger = require('morgan');
var compression = require('compression');
var methodOverride = require('method-override');
var session = require('express-session');
var flash = require('express-flash');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var dotenv = require('dotenv');

// Load environment variables from .env file
dotenv.load();

// Controllers
var HomeController = require('./controllers/home');
var GalaDetailsController = require('./controllers/galaDetails');
var GalaCommitteeController = require('./controllers/galaCommittee');
var GalaSponsorsController = require('./controllers/galaSponsors');
var GCRCController = require('./controllers/gcrc');
var FocusController = require('./controllers/focus');
var contactController = require('./controllers/contact');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(methodOverride('_method'));
app.use(session({ secret: process.env["SESSION_SECRET"], resave: true, saveUninitialized: true }));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', HomeController.index);
app.get('/gala', GalaDetailsController.index);
app.get('/gala/details', GalaDetailsController.index);
app.get('/gala/committee', GalaCommitteeController.index);
app.get('/gala/sponsors', GalaSponsorsController.index);
app.get('/gcrc', GCRCController.index);
app.get('/focus', FocusController.index);
app.get('/contact', contactController.contactGet);
app.post('/contact', contactController.contactPost);

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
