/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('galaDetails', {
    title: '2018 GALA - Details',
    superTitle: 'gala',
    pageID: 'details',
    hasTabMenu: true,
    hasHero: true
  });
};
