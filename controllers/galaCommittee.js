/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('galaCommittee', {
    title: '2018 GALA - Committee',
    superTitle: 'gala',
    pageID: 'committee',
    hasTabMenu: true,
    hasHero: true
  });
};
