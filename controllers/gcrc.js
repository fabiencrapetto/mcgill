/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('gcrc', {
    title: '2018 GALA - GCRC',
    superTitle: 'gcrc'
  });
};
