/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('home', {
    title: '2018 GALA - McGill Rosalind and Morris Goodman Cancer Research Centre',
    superTitle: 'home',
    hasHero: true
  });
};
