/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('galaSponsors', {
    title: '2018 GALA - Sponsors and Donors',
    superTitle: 'gala',
    pageID: 'sponsors',
    hasTabMenu: true,
    hasHero: true
  });
};
