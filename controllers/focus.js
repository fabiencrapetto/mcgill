/**
 * GET /
 */
exports.index = function(req, res) {
  res.render('focus', {
    title: '2018 GALA - FOCUS',
    superTitle: 'focus',
  });
};
