$(function() {

  //Parallax manager
  $(window).on("scroll", function(a) {
    window.requestAnimationFrame(function () {
      $(".parallax-bckgrd, .parallax-object").each(function(e,i){
        var $c = $(this),
            options = {
              offset: 0,
              invert: false,
              speed: 2,
            },
            s_calc = function(s){
              return s/10* -1;
            };

        var o = $c.is("[data-offset]")?$c.data("offset"):options.offset;
        var i = $c.is("[data-invert]")?true:options.invert;
        var s = $c.is("[data-speed]")?$c.data("speed"):options.speed;

        var scrolled = $(window).scrollTop() + o - $c.parent().offset().top;
        $c.css({
          'transform': 'translate3d(0,' + (scrolled * s_calc(s) * (i?-1:1)) + 'px, 0)'
        });
      });
    });
  });

  
});
